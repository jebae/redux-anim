const babelJest = require('babel-jest');
const babelConfig = require('../paths').babelConfig;

module.exports = babelJest.createTransformer(babelConfig);