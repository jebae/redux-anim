var paths = require('../paths');

module.exports = {
	verbose: true,
	rootDir: paths.rootDir,
	moduleFileExtensions: [
		'ts', 'tsx', 'js', 'jsx'
	],
	testRegex: '(/__tests__/.*|\\.(test|spec))\\.(ts|tsx|js)$',
	setupTestFrameworkScriptFile: '<rootDir>/configs/jest/enzyme.config.js',
	setupFiles: ['raf/polyfill'],
	transform: {
		'^.+\\.jsx?$': '<rootDir>/configs/jest/fileTransform.js',
		'^.+\\.tsx?$': '<rootDir>/node_modules/ts-jest/preprocessor.js',
		'^.+\\.scss$': '<rootDir>/configs/jest/cssTransform.js'
	},
	globals: {
		'ts-jest': {
			tsConfigFile: './configs/tsconfig.json'
		}
	}
};