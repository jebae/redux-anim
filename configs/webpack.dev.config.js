var webpack = require('webpack');
var paths = require('./paths');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: paths.rootDir + '/src/index.js',
	output: {
		path: '/',
		publicPath: '/',
		filename: 'bundle.js'
	},
	resolve: {
		extensions: [
			'.js', '.jsx', '.ts', '.tsx'
		]
	},
	devServer: {
		contentBase: paths.rootDir + '/public',
		port: 3000,
		historyApiFallback: true
	},
	module: {
		loaders: [
			{
				test: /.jsx?$/,
				loader: 'babel-loader',
				exclude: paths.rootDir + '/node_modules/',
				query: Object.assign({ cacheDirectory: true }, paths.babelConfig)
			},
			{
				test: /.tsx?$/,
				exclude: paths.rootDir + '/node_modules/',
				use: [
					{
						loader: 'babel-loader',
						query: Object.assign({ cacheDirectory: true }, paths.babelConfig)
					},
					{
						loader: 'awesome-typescript-loader',
						query: {
							configFileName: paths.rootDir + '/configs/tsconfig.json'
						}
					}
				]
			},
			{
				test: /.scss$/,
				exclude: paths.rootDir + '/node_modules/',
				use: [
					'style-loader',
					{
						loader: 'css-loader',
						options: {
							localIdentName: '[path][name]__[local]__[hash:base64:5]'
						}
					},
					'sass-loader'
				]
			}
		]
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new HtmlWebpackPlugin({
			template: paths.rootDir + '/public/index.html'
		})
	]
};