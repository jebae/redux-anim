import * as React from 'react';
import { Header } from './components';
import { PostContainer } from './containers';

class App extends React.Component{
	render(){
		return (
			<div>
				<Header/>
				<PostContainer/>
			</div>
		);
	}
};

export default App;