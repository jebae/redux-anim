import * as React from 'react';
import { CommentState } from '../types/index.d';

interface CommentProps extends CommentState {

};

const Comment:React.SFC<CommentProps> = ({ title, body }) => {
	return (
		<li className="Comment">
			<p>
					<b>{ title }</b> { body }
			</p>
		</li>
	);
};

export default Comment;