import * as React from 'react';
import { CommentListState } from '../types/index.d';
import { Comment } from './';

interface CommentListProps extends CommentListState {

};

const CommentList: React.SFC<CommentListProps> = ({ comments }) => {
	const commentsComponents = comments.map((v, i) => (
		<Comment 
			key={ i }
			title={ v.title } 
			body={ v.body }
		/>
	));
	
	return (
		<ul className="CommentList">
			{ commentsComponents }
		</ul>
	);
};

export default CommentList;