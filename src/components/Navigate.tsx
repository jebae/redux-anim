import * as React from 'react';
import { NavigateState, PostAction } from '../types/index.d';
import '../stylesheets/components/Navigate.scss';

interface NavigateProps extends NavigateState {
	onNextPost: () => PostAction,
	onPrevPost: () => PostAction
};

const Navigate: React.SFC<NavigateProps> = ({ page, onNextPost, onPrevPost }) => {
	const nextText = 'Next';
	const prevText = 'Previous';

	return (
		<div className="Navigate">
			<button 
				className="Navigate-Button-prev"
				onClick={ onPrevPost }
			>
				{ prevText }
			</button>
			<div className="Navigate-page-num">
        { page }
      </div>
			<button 
				className="Navigate-Button-next"
				onClick={ onNextPost }
			>
				{ nextText }
			</button>
		</div>
	)
};

export default Navigate;