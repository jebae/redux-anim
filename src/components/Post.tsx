import * as React from 'react';
import { PostState } from '../types/index.d';
import { CommentList } from './';

interface PostProps extends PostState {

};

const Post: React.SFC<PostProps> = ({ title, body, comments }) => {
	return (
		<div className="Post">
			<h1>{ title }</h1>
			<p>
				{ body }
			</p>
			<CommentList comments={ comments }/>
		</div>
	);
};

export default Post;