import * as React from 'react';
import { PostWrapperState } from '../types/index.d';
import { Post } from './';
import { NavigateContainer, WarningContainer } from '../containers';
import '../stylesheets/components/PostWrapper.scss';

interface PostWrapperProps extends PostWrapperState {

};

const PostWrapper: React.SFC<PostWrapperProps> = ({ data, page }) => {
	const { title, body, comments } = data;
	
	return (
		<div className="PostWrapper">
			<NavigateContainer/>
			<Post 
				title={ title } 
				body={ body }
				comments={ comments }
			/>
			<WarningContainer/>
		</div>
	);
};

export default PostWrapper;