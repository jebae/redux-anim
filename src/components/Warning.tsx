import * as React from 'react';
import '../stylesheets/components/Warning.scss';

interface WarningProps {
	message: string,
	closing: Boolean
};

const Warning: React.SFC<WarningProps> = ({ message, closing }) => {
	const animation = closing ? 'BounceOut' : 'BounceIn';
	const className = `animated Warning ${ animation }`;

	return (
		<div className="Warning-wrapper">
			<div className={ className }>
				{ message }
			</div>
		</div>
	);
};

export default Warning;