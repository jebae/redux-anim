import * as React from 'react';
import { mount, shallow } from 'enzyme';
import CommentList from '../CommentList';
import Comment from '../Comment';

let CommentListComponent, testComments;

beforeEach(() => {
	testComments = [
		{ title: 'Test1 title', body: 'Test1 body' },
		{ title: 'Test2 title', body: 'Test2 body' },
		{ title: 'Test3 title', body: 'Test3 body' }
	];

	CommentListComponent = mount(
		<div>
			<CommentList comments={ testComments }/>
		</div>
	);
});

describe('CommentList', () => {
	it('should be rendered', () => {
		expect(CommentListComponent.find(CommentList).length).toEqual(1);
	});

	it('should have comments props', () => {
		expect(CommentListComponent.find(Comment).length).toEqual(3);
	});
});