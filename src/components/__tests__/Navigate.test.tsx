import * as React from 'react';
import { shallow, mount } from 'enzyme';
import Navigate from '../Navigate';

let NavigateComponent, testText;

beforeEach(() => {
	testText = 'Test Text';
	NavigateComponent = mount(
		<Navigate page={ 1 }/>
	);
});

describe('Navigate', () => {
	it('should have two button', () => {
		expect(NavigateComponent.find('button').length).toEqual(2);
		const nextBtn = NavigateComponent.find('.Navigate-Button-next');
		const prevBtn = NavigateComponent.find('.Navigate-Button-prev');
		expect(nextBtn.text()).toEqual('Next');
		expect(prevBtn.text()).toEqual('Previous');
	});

	it('should have page props', () => {
		expect(NavigateComponent.props().page).toEqual(1);
	});
});