import * as React from 'react';
import Post from '../Post';
import { mount } from 'enzyme';

let PostComponent, testTitle, testBody, testComments;

beforeEach(() => {
	testTitle = 'test title';
	testBody = 'test body';
	testComments = [
		{ title: 'Test Comment', body: 'test comment body' },
		{ title: 'Test Comment', body: 'test comment body' },
		{ title: 'Test Comment', body: 'test comment body' }
	];
	PostComponent = mount(
		<Post 
			title={ testTitle } 
			body={ testBody }
			comments={ testComments }
		/>
	);
});

describe('Post', () => {
	it('should be rendered', () => {
		expect(PostComponent.find(Post).length).toEqual(1);
	})

	it('should have comment', () => {
		expect(PostComponent.props().comments.length).toEqual(3);
	})
});