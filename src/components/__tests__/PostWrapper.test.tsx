import * as React from 'react';
import { shallow, mount } from 'enzyme';
import PostWrapper from '../PostWrapper';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

let PostWrapperComponent, testData, mockStore, store;

beforeEach(() => {
	mockStore = configureMockStore();
	store = mockStore({});
	testData = {
		title: 'Test Text',
		body: 'Test Text',
		comments: []
	};
	PostWrapperComponent = shallow(
		<Provider store={ store }>
			<PostWrapper 
				data={ testData }
				page={ 1 }
			/>
		</Provider>
	).find(PostWrapper);
});

describe('PostWrapper', () => {
	it('should contains title', () => {
		expect(PostWrapperComponent.props().data).toEqual(testData);
	});
});