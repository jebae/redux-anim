import * as React from 'react';
import { mount } from 'enzyme';
import Warning from '../Warning';

let WarningComponent, testMessage;

beforeAll(() => {
	testMessage = 'Test Warning';
	WarningComponent = mount(
		<Warning message={ testMessage } closing={ false }/>
	);
});

describe('Warning Component', () => {
	it('should render component', () => {
		expect(WarningComponent.find('.Warning').length).toEqual(1);
	});

	it('should have message props', () => {
		expect(WarningComponent.props().message).toEqual(testMessage);
	});

	it('should have class BounceIn', () => {
		expect(WarningComponent.find('.Warning').hasClass('BounceIn')).toBe(true);
	});

	it('should have class BounceOut when closing', () => {
		WarningComponent.setProps({ closing: true });
		expect(WarningComponent.find('.Warning').hasClass('BounceOut')).toBe(true);
	});
});