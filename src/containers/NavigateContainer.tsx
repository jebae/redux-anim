import * as React from 'react';
import { connect } from 'react-redux';
import { Navigate } from '../components';
import { 
	NavigateState, 
	PostAction, 
	WarningState, 
	ApplicationState 
} from '../types/index.d';
import * as PostActions from '../modules/Post';
import * as WarningActions from '../modules/Warning';

interface NavigateProps extends NavigateState {
	warning: WarningState,
	onNextPost: () => PostAction,
	onPrevPost: () => PostAction
};

export class NavigateContainer extends React.Component<NavigateProps> {
	render(){
		const { page, onNextPost, onPrevPost } = this.props;
		return (
			<Navigate 
				page={ page }
				onNextPost={ onNextPost }
				onPrevPost={ onPrevPost }
			/>
		);
	}
};

const mapStateToProps = (state: ApplicationState) => ({
	page: state.Post.page
});

const mapDispatchToProps = (dispatch) => ({
	onNextPost: () => dispatch(PostActions.nextPost()),
	onPrevPost: () => dispatch(PostActions.prevPost())
});

export default connect(mapStateToProps, mapDispatchToProps)(NavigateContainer);