import * as React from 'react';
import { connect } from 'react-redux';
import { PostWrapper, Warning } from '../components';
import {
	WarningState,
	PostContainerState,
	ApplicationState
} from '../types/index.d';
import * as PostActions from '../modules/Post';
import * as WarningActions from '../modules/Warning';

interface PostContainerProps extends PostContainerState {
	error: Boolean,
	warning: WarningState,
	onGetPost: (postId: number) => (dispatch: any) => Promise<any>,
	onWarning: (message: string) => (dispatch: any) => Promise<any>
};

export class PostContainer extends React.Component<PostContainerProps> {
	componentDidMount(){
		const { page } = this.props;
		this.getPost(page);
	};

	componentWillReceiveProps(nextProps){
		const { page: nextPage, error: nextError } = nextProps;
		const { error, warning, onWarning } = this.props;

		if (nextPage != this.props.page){
			this.getPost(nextPage);
		};

		if (!this.props.error && nextError){
			if (!(warning.visible || warning.closing)){
				onWarning('No Post');
			};
		};
	};

	async getPost(postId: number){
		const { onGetPost, warning } = this.props;
		try {
			await onGetPost(postId);
		} catch (e) {
			e => console.log(e);
		};
	};

	render(){
		const { data, page, warning } = this.props;
		return (
			<PostWrapper
				data={ data }
				page={ page }
			/>
		);
	};
};

const mapStateToProps = (state: ApplicationState) => ({
	data: state.Post.data,
	page: state.Post.page,
	error: state.Post.error,
	warning: state.Warning,
});

const mapDispatchToProps = (dispatch) => ({
	onGetPost: (postId: number) => dispatch(PostActions.getPost(postId)),
	onWarning: (message: string) => dispatch(WarningActions.postWarning(message))
});

export default connect(mapStateToProps, mapDispatchToProps)(PostContainer);