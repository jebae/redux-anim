import * as React from 'react';
import { WarningState, ApplicationState } from '../types/index.d';
import { connect } from 'react-redux';
import { Warning } from '../components';

interface WarningContainerProps extends WarningState {

};

export class WarningContainer extends React.Component<WarningContainerProps> {
	render(){
		const { visible, message, closing } = this.props;
		if (!visible){
			return null;
		};
		return (
			<Warning message={ message } closing={ closing }/>
		);
	};
};

const mapStateToProps = (state: ApplicationState) => {
	return { ...state.Warning };
};

export default connect(mapStateToProps)(WarningContainer);