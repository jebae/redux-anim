import * as React from 'react';
import configureMockStore from 'redux-mock-store';
import { thunkMiddleware } from '../../lib/middlewares';
import { mount, shallow } from 'enzyme';
import { Provider } from 'react-redux';
import ConnectedNavigateContainer, { NavigateContainer } from '../NavigateContainer';
import { Navigate } from '../../components';
import { TypeKeys } from '../../modules/Post';

let mockStore, store, ConnectedComponent, NavigateComponent;

beforeEach(() => {
	const middlewares = [thunkMiddleware];
	mockStore = configureMockStore(middlewares);
	store = mockStore({ Post: { page: 1 } });
	ConnectedComponent = mount(
		<ConnectedNavigateContainer store={ store }/>
	);
	NavigateComponent = ConnectedComponent.find(NavigateContainer);
});

describe('Navigate Container', () => {
	it('should be rendered', () => {
		expect(NavigateComponent.length).toBe(1);
	});

	it('should return NextPost action', () => {
		NavigateComponent.find('.Navigate-Button-next').simulate('click');
		expect(store.getActions()).toEqual([
			{ type: TypeKeys.NEXT_POST }
		])
	});

	it('should return PrevPost action', () => {
		NavigateComponent.find('.Navigate-Button-prev').simulate('click');
		expect(store.getActions()).toEqual([
			{ type: TypeKeys.PREV_POST }
		])
	});
});