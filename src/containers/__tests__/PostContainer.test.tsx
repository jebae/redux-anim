jest.mock('../../services/Post');
import * as React from 'react';
import { mount, shallow } from 'enzyme';
import configureMockStore from 'redux-mock-store';
import ConnectedPostContainer, { PostContainer } from '../PostContainer';
import { thunkMiddleware } from '../../lib/middlewares';
import { Provider } from 'react-redux';
import { initialState as WarningInitialState } from '../../modules/Warning';
import { CommentState, PostState } from '../../types/index.d';
import { getPostAPI, getCommentsAPI } from '../../services';
import { 
	initialState as PostInitialState,
	TypeKeys as PostTypeKeys
} from '../../modules/Post';
import { 
	TypeKeys as WarningTypeKeys
} from '../../modules/Warning';

let ConnectedComponent, 
	PostComponent,
	mockStore,
	store,
	title,
	body,
	comments,
	expectedComments,
	showErrorProps;
	

beforeEach(() => {
	mockStore = configureMockStore([thunkMiddleware]);
	store = mockStore({
		Post: PostInitialState,
		Warning: WarningInitialState
	});
	title = 'Test title';
	body = 'Test body';
	comments = [
		{ name: 'comment1 name', body: 'comment1 body' },
		{ name: 'comment2 name', body: 'comment2 body' },
		{ name: 'comment3 name', body: 'comment3 body' }
	];

	expectedComments = comments.map((v, i) => {
		return { title: v.name, body: v.body };
	});

	getPostAPI.mockReturnValue(
		new Promise((resolve, reject) => {
			resolve({ data: { title, body } });
		})
	);

	getCommentsAPI.mockReturnValue(
		new Promise((resolve, reject) => {
			resolve({ data: comments });
		})
	);

	ConnectedComponent = mount(
		<Provider store={ store }>
			<ConnectedPostContainer/>
		</Provider>
	);
	PostComponent = ConnectedComponent.find(PostContainer);

	showErrorProps = {
		data: PostInitialState.data,
		page: PostInitialState.page,
		warning: WarningInitialState,
		error: true
	}
});

describe('Post Container', () => {
	it('should be rendered', () => {
		expect(PostComponent.length).toEqual(1);
	});

	it('should get post when mounted', () => {
		expect(store.getActions()).toEqual([
			{ type: PostTypeKeys.GET_POST_PENDING },
			{ 
				type: PostTypeKeys.GET_POST_SUCCESS,
				data: { title, body, comments: expectedComments }
			}
		])
	});

	it('should get post when page is changed', () => {
		store.clearActions();
		const nextProps = {
			data: PostInitialState.data,
			page: 2,
			error: false
		};
		PostComponent.instance().componentWillReceiveProps(nextProps);
		expect(store.getActions()).toEqual([
			{ type: PostTypeKeys.GET_POST_PENDING }
		]);
	});

	it('should show warning when error on post', () => {
		store.clearActions();

		PostComponent.instance().componentWillReceiveProps(showErrorProps);
		expect(store.getActions()).toEqual([
			{ type: WarningTypeKeys.SHOW_WARNING, message: 'No Post' }
		]);
	});

	it('should not show warning when warning is already on show', () => {
		store = mockStore({
			Post: PostInitialState,
			Warning: { ...WarningInitialState, visible: true }
		});
		PostComponent = mount(
			<Provider store={ store }>
				<ConnectedPostContainer/>
			</Provider>
		).find(PostContainer);
		store.clearActions();

		PostComponent.instance().componentWillReceiveProps(showErrorProps);
		expect(store.getActions().length).toBe(0);
	});
});