import * as React from 'react';
import { WarningContainer } from '../WarningContainer';
import { Warning } from '../../components';
import { shallow } from 'enzyme';

let Component;

beforeEach(() => {
	Component = shallow(
		<WarningContainer 
			visible={ false }
			closing={ false }
			message=''
		/>
	)
});

describe('WarningContainer', () => {
	it('should not have warning when init', () => {
		expect(Component.find(Warning).length).toBe(0);
	});

	it('should have warning when visible=true', () => {
		Component.setProps({ visible: true });
		expect(Component.find(Warning).length).toBe(1);
	});
});