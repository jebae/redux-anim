import ReactDOM from 'react-dom';
import * as React from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { thunkMiddleware } from './lib/middlewares';
import App from './App';
import './stylesheets/main.scss';
import Reducer from './modules';

const middlewares = applyMiddleware(thunkMiddleware);
const store = createStore(Reducer, middlewares);

ReactDOM.render(
	<Provider store={ store }>
		<App/>
	</Provider>,
	document.getElementById('root')
);