import { PostAction, PostContainerState as _PostContainerState } from '../types/index.d';
import { getPostAPI, getCommentsAPI } from '../services';

interface PostContainerState extends _PostContainerState {
	pending: boolean,
	error: boolean
};

// actions
export enum TypeKeys {
	GET_POST_SUCCESS='redux-anim/post/GET_SUCCESS',
	GET_POST_FAILURE='redux-anim/post/GET_FAILURE',
	GET_POST_PENDING='redux-anim/post/GET_PENDING',
	NEXT_POST='redux-anim/post/NEXT_POST',
	PREV_POST='redux-anim/post/PREV_POST'
};

// initial state
export const initialState:PostContainerState = {
	page: 1,
	data: {
		title: '',
		body: '',
		comments: []
	},
	pending: false,
	error: false
};

// reducer
export default (
	state: PostContainerState=initialState,
	action: PostAction
): PostContainerState => {
	switch (action.type){
		case TypeKeys.NEXT_POST:
			return {
				...state,
				page: state.page + 1
			};

		case TypeKeys.PREV_POST:
			return {
				...state,
				page: state.page - 1
			};

		case TypeKeys.GET_POST_PENDING:
			return {
				...state,
				pending: true,
				error: false
			};

		case TypeKeys.GET_POST_SUCCESS:
			const { title, body, comments } = action.data;
			return {
				...state,
				pending: false,
				data: { title, body, comments }
			};
		
		case TypeKeys.GET_POST_FAILURE:
			return {
				...state,
				pending: false,
				error: true
			};
		default:
			return state;
	}
};

// action creators
export const getPost = (postId: number) => (dispatch): Promise<any> => {
	dispatch({ type: TypeKeys.GET_POST_PENDING });
	
	return Promise.all([getPostAPI(postId), getCommentsAPI(postId)])
		.then(
			responses => {
				const { title, body } = responses[0].data;
				const comments = responses[1].data.map((v, i) => {
					return {
						title: v.name,
						body: v.body
					}
				});
				dispatch({
					type: TypeKeys.GET_POST_SUCCESS,
					data: { title, body, comments }
				})
			}
		)
		.catch(
			e => {
				dispatch({
					type: TypeKeys.GET_POST_FAILURE
				})
			}
		);
};

export const nextPost = ():PostAction => ({
	type: TypeKeys.NEXT_POST
});

export const prevPost = ():PostAction => ({
	type: TypeKeys.PREV_POST
});