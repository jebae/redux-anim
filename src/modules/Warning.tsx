import { WarningAction, WarningState } from '../types/index.d';

// actions
export enum TypeKeys {
	SHOW_WARNING='redux-anim/warning/SHOW_WARNING',
	HIDE_WARNING='redux-anim/warning/HIDE_WARNING',
	HIDE_COMPLETE_WARNING='redux-anim/warning/HIDE_COMPLETE_WARNING'
};

// initial state
export const initialState: WarningState = {
	visible: false,
	closing: false,
	message: ''
};

// reducer
export default (
	state: WarningState=initialState,
	action: WarningAction
): WarningState => {
	switch (action.type) {
		case TypeKeys.SHOW_WARNING:
			return {
				...state,
				visible: true,
				message: action.message
			};

		case TypeKeys.HIDE_WARNING:
			return {
				...state,
				closing: true
			};

		case TypeKeys.HIDE_COMPLETE_WARNING:
			return {
				...state,
				visible: false,
				closing: false,
				message: ''
			};
		
		default:
			return state;
	};
};

// action creator
export const postWarning = (message: string) => (dispatch): Promise<any> => {
	return new Promise((resolve, reject) => {
		dispatch({
			type: TypeKeys.SHOW_WARNING,
			message
		});
		setTimeout(function(){
			resolve();
		}, 1000);
	})
	.then((): Promise<any> => {
		return new Promise((resolve, reject) => {
			dispatch({ type: TypeKeys.HIDE_WARNING });
			setTimeout(function(){
				resolve();
			}, 1000);
		});
	})
	.then(() => {
		dispatch({ type: TypeKeys.HIDE_COMPLETE_WARNING });
	})
	.catch(
		e => {
			// what to do?
			console.log(e);
		}
	);
};