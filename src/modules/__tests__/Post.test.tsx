jest.mock('../../services/Post');
import Reducer, {
	initialState, getPost, TypeKeys, nextPost, prevPost
} from '../Post';
import configureMockStore from 'redux-mock-store';
import { thunkMiddleware } from '../../lib/middlewares';
import { PostAction } from '../../types/index.d';
import { getPostAPI, getCommentsAPI } from '../../services';

let store;

beforeEach(() => {
	const middlewares = [thunkMiddleware];
	const mockStore = configureMockStore(middlewares);
	store = mockStore({});
});

describe('Post Reducer', () => {
	it('should return initialState', () => {
		const noAction:PostAction = { 
			type: undefined, 
			data: { title: '', body: '', comments: [] }
		};
		expect(Reducer(initialState, noAction)).toEqual(initialState);
	});

	it('should return increased page', () => {
		expect(Reducer(initialState, nextPost())).toEqual(
			{ ...initialState, page: initialState.page + 1 }
		);
	});

	it('should return decresed page', () => {
		expect(Reducer({
			 ...initialState, page: 2 
		}, prevPost())).toEqual(
			{ ...initialState }
		);
	});

	it('should success with post api', async () => {
		const title = 'Test Title';
		const body = 'Test Body';
		const comments = [
			{ name: 'comment1 name', body: 'comment1 body' },
			{ name: 'comment2 name', body: 'comment2 body' },
			{ name: 'comment3 name', body: 'comment3 body' }
		]
		getPostAPI.mockReturnValueOnce(
			new Promise((resolve, reject) => {
				resolve({ data: { title, body } });
			})
		);
		getCommentsAPI.mockReturnValueOnce(
			new Promise((resolve, reject) => {
				resolve({ data: comments });
			})
		);
		await store.dispatch(getPost(1));
		const expectedComments = comments.map((v, i) => {
			return {
				title: v.name, body: v.body
			};
		});
		expect(store.getActions()).toEqual([
			{ type: TypeKeys.GET_POST_PENDING },
			{
				type: TypeKeys.GET_POST_SUCCESS,
				data: { title, body, comments: expectedComments }
			}
		]);
	});

	it('should success with post api', async () => {
		getPostAPI.mockReturnValueOnce(
			new Promise((resolve, reject) => {
				reject();
			})
		);
		await store.dispatch(getPost(1));
		expect(store.getActions()).toEqual([
			{ type: TypeKeys.GET_POST_PENDING },
			{ type: TypeKeys.GET_POST_FAILURE }
		]);
	});
});