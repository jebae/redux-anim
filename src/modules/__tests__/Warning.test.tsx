jest.useFakeTimers();
import Reducer, { initialState, postWarning, TypeKeys } from '../Warning';
import configureMockStore from 'redux-mock-store';
import { thunkMiddleware } from '../../lib/middlewares';
import { TestPromise } from '../../utils/JestUtils';

let store, originPromise, testMessage;

beforeEach(() => {
	originPromise = Promise;
	global.Promise = TestPromise;
	
	testMessage = 'Test Message';
	const middlewares = [thunkMiddleware];
	const mockStore = configureMockStore(middlewares);
	store = mockStore({});
});

describe('Warning reducer', () => {
	it('should return initialState', () => {
		const noAction = { type: undefined };
		expect(Reducer(initialState, noAction)).toEqual(initialState);
	});

	it('should return SHOW, HIDE, HIDE_COMPLETE actions', async () => {
		await store.dispatch(postWarning(testMessage));
		
		expect(store.getActions()).toEqual([
			{ type: TypeKeys.SHOW_WARNING, message: testMessage },
			{ type: TypeKeys.HIDE_WARNING },
			{ type: TypeKeys.HIDE_COMPLETE_WARNING }
		]);

		const showState = Reducer(initialState, store.getActions()[0]);
		const closingState = Reducer(showState, store.getActions()[1]);
		const completeState = Reducer(closingState, store.getActions()[2]);

		expect(showState).toEqual({
			...initialState,
			message: testMessage;
			visible: true
		});

		expect(closingState).toEqual({
			...showState,
			message: testMessage,
			closing: true
		});

		expect(completeState).toEqual(initialState);
	});
});

afterEach(() => {
	global.Promise = originPromise;
});