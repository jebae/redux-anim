import { combineReducers } from 'redux';
import PostReducer from './Post';
import WarningReducer from './Warning';

export default combineReducers({
	Post: PostReducer,
	Warning: WarningReducer
});