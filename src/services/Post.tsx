import axios from 'axios';

export const getPostAPI = (postId: number): Promise<any> => {
	return axios.get(`https://jsonplaceholder.typicode.com/posts/${postId}`);
};

export const getCommentsAPI = (postId: number): Promise<any> => {
	return axios.get(`https://jsonplaceholder.typicode.com/posts/${postId}/comments`);
};