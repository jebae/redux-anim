import { getPostAPI, getCommentsAPI } from './Post';

export {
	getPostAPI,
	getCommentsAPI
};