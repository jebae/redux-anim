import { Action } from './__redux__.d';

// action
export interface PostAction extends Action {
	data?: PostState
};

// state and props
export interface NavigateState {
	page: number
};

export interface CommentState {
	title: string,
	body: string
};

export interface CommentListState {
	comments: CommentState[]
};

export interface PostState extends CommentListState {
	title: string,
	body: string
};

export interface PostWrapperState extends NavigateState {
	data: PostState
};

export interface PostContainerState extends PostWrapperState {
	error: Boolean
};