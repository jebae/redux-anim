import { Action } from './__redux__.d';

export interface WarningAction extends Action {
	message?: string
};

export interface WarningState {
	visible: Boolean,
	closing: Boolean,
	message: string
};