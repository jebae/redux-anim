import { PostContainerState } from './Post.d';
import { WarningState } from './Warning.d';

export {
	PostAction,
	PostState,
	NavigateState,
	PostWrapperState,
	PostContainerState,
	CommentListState,
	CommentState
} from './Post.d';

export {
	WarningAction,
	WarningState
} from './Warning.d'

export interface ApplicationState {
	Post: PostContainerState,
	Warning: WarningState
};