export class TestPromise extends Promise<any> {
	constructor(executor){
		super((resolve, reject) => {
			const result = executor(resolve, reject);
			jest.runOnlyPendingTimers();
			return result;
		});
	};
};